<!-- All of the process -->
<?php

    include "koneksi.php";

    $update = false;
    $id = "";
    $foto= "";
    $nama= "";
    $nip= "";
    $prodi="";
    $fakultas="";

    if(isset($_POST["submit"])){
        $foto = $_POST["foto_dosen"];
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        $sql = "INSERT INTO `dosen`(`foto_dosen`, `nip_dosen`, `nama_dosen`, `prodi`, `fakultas`) VALUES ('$foto','$nip','$nama','$prodi','$fakultas')";
    
        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    
    }

    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `dosen` WHERE id_dosen = $id";
        $q1 = mysqli_query($koneksi, $sql);
        $row = mysqli_fetch_array($q1);

        $foto = $row["foto_dosen"];
        $nama = $row["nama_dosen"];
        $nip = $row["nip_dosen"];
        $prodi = $row["prodi"];
        $fakultas = $row["fakultas"];


        if($nip == " "){
            $status = "data kosong";
        }
    }

    if(isset($_POST["edit"])){
        $id = $_POST["id_dosen"];
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        if($_FILES["foto"]["error"] == 4){
            echo"File Error!";
        }else{
            $nama_foto = $_FILES["foto"]["name"];
            $ambil = $_FILES["foto"]["tmp_name"] ;
            $tujuan = "img/".$nama_foto;
            $move = move_uploaded_file($ambil,$tujuan);
        }

        $sql = "UPDATE `dosen` SET `foto_dosen`='$nama_foto',`nip_dosen`='$nip',`nama_dosen`='$nama',`prodi`='$prodi',`fakultas`='$fakultas' WHERE id_dosen = $id";

        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }

    if(isset($_GET["delete"])){
        $id = $_GET["delete"];
        $sql = "DELETE FROM `dosen` WHERE id_dosen = $id";

        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Dosen</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <!-- Tampilkan Data di Database tabel dosen.. -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Links -->
    <ul class="navbar-nav">
    <li class="nav-item">
    <a class="nav-link" href="dosen.php">Form Dosen</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="kelas.php">Form Kelas</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jadwal.php">Form Jadwal</a>
    </li>
    </ul>
</nav>
    <div class="container col-8">
    <?php
        include "koneksi.php";
        $sql = "SELECT * FROM dosen";
        $result = mysqli_query($koneksi,$sql);
    ?>
    <div class="row justify-content-center">
    <table border="2" cellpadding="3">
        <thead>
            <tr>
                <th>Foto Dosen</th>
                <th>Nama Dosen</th>
                <th>NIP</th>
                <th>Prodi</th>
                <th>Fakultas</th>
                <th>Action</th>
            </tr>
        </thead>
        <?php while($row = $result->fetch_assoc()): ?>
            <tr>
                <td><img src="img/<?php echo $row["foto_dosen"];?>" width="80px" alt="Foto Dosen"></td>
                <td><?php echo $row["nama_dosen"];?></td>
                <td><?php echo $row["nip_dosen"];?></td>
                <td><?php echo $row["prodi"];?></td>
                <td><?php echo $row["fakultas"];?></td>
                <td>
                    <a href="dosen.php?edit=<?php echo $row["id_dosen"];?>" class="btn btn-primary" >Edit</a>
                    <a href="dosen.php?delete=<?php echo $row["id_dosen"];?>" class="btn btn-danger">Hapus</a>
                </td>
            </tr>
        <?php endwhile;?>
    </table>
    </div>
    </div>

    <?php
        function pre_r($array){
            echo "<pre>";
            print_r($array);
            echo"<pre>";
        }
    ?>
    
    <!-- Form Input Tabel Dosen -->
    <form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id_dosen" value="<?php echo $id; ?>">
    <label for="formGroupExampleInput" for="Nama Dosen">Nama Dosen</label><br>
    <input class="form-control" type="text" name="nama_dosen" value="<?php echo $nama; ?>" id="nama_dosen" required><br>

    <label for="formGroupExampleInput" for="NIP">NIP Dosen</label><br>
    <input class="form-control" type="text" name="nip_dosen" value="<?php echo $nip; ?>" id="nip_dosen" required><br>

    <label for="formGroupExampleInput" for="FILE">Foto</label><br>
    <input class="form-control" type="file" name="foto" value="<?php echo $nama_foto; ?>" id="foto" required><br>

    <label for="formGroupExampleInput"for="prodi">Program Studi</label><br>
    <select class="form-control" name="prodi" id="prodi" required>
        <option value="Sistem Informasi"<?php if($prodi == "Sistem informasi") echo "dipilih"?>>Sistem Informasi</option>
        <option value="Pendidikan Teknik Informatika" <?php if($prodi == "Pendidikan Teknik Informatika") echo "dipilih"?>>Pendidikan Teknik Informatika</option>
        <option value="Pendidikan Ekonomi" <?php if($prodi == "Pendidikan Ekonomi") echo "dipilih"?>>Pendidikan Ekonomi</option>
        <option value="S1 Manajemen" <?php if($prodi == "S1 Manajemen") echo "dipilih"?>>S1 Manajemen</option>
        <option value="Pendidikan Matematika" <?php if($prodi == "Pendidikan Matematika") echo "dipilih"?>>Pendidikan Matematika</option>
        <option value="Pendidikan Olahraga" <?php if($prodi == "Pendidikan Olahraga") echo "dipilih"?>>Pendidikan Olahraga</option>
        <option value="Kedokteran" <?php if($prodi == "Kedokteran") echo "dipilih"?>>Kedokteran</option>
        <option value="Pendidikan Sekolah Dasar" <?php if($prodi == "Pendidikan Sekolah Dasar") echo "dipilih"?>>Pendidikan Sekolah Dasar</option>
        <option value="Ilmu Hukum" <?php if($prodi == "Ilmu Hukum") echo "dipilih"?>>Ilmu Hukum</option>
        <option value="Pendidikan Bahasa Inggris" <?php if($prodi == "Pendidikan Bahasa Inggris") echo "dipilih"?>>Pendidikan Bahasa Inggris</option>
        <option value="Pendidikan Seni Rupa" <?php if($prodi == "Pendidikan Seni Rupa") echo "dipilih"?>>Pendidikan Seni Rupa</option>
    </select><br>
    
    <label for="fakultas">Fakultas</label><br>
    <select name="fakultas" id="fakultas" required>
        <option value="Fakultas Teknik dan Kejuruan" <?php if($fakultas == "Fakultas Teknik dan Kejuruan") echo "dipilih"?>>Fakultas Teknik dan Kejuruan</option>
        <option value="Fakultas Kedokteran" <?php if($fakultas == "Fakultas Kedokteran") echo "dipilih"?>>Fakultas Kedokteran</option>
        <option value="Fakultas Matematika dan IPA" <?php if($fakultas == "Fakultas Matematika dan IPA") echo "dipilih"?>>Fakultas Matematika dan IPA</option>
        <option value="Fakultas Ilmu Pendidikan" <?php if($fakultas == "Fakultas Ilmu Pendidikan") echo "dipilih"?>>Fakultas Ilmu Pendidikan</option>
        <option value="Fakultas Hukum dan Ilmu Sosial" <?php if($fakultas == "Fakultas Hukum dan Ilmu Sosial") echo "dipilih"?>>Fakultas Hukum dan Ilmu Sosial</option>
        <option value="Fakultas Ekonomi" <?php if($fakultas == "Fakultas Ekonomi") echo "dipilih"?>>Fakultas Ekonomi</option>
        <option value="Fakultas Bahasa dan Seni" <?php if($fakultas == "Fakultas Bahasa dan Seni") echo "dipilih"?>>Fakultas Bahasa dan Seni</option>
        <option value="Fakultas Olahraga dan Kesehatan" <?php if($fakultas == "Fakultas Olahraga dan Kesehatan") echo "dipilih"?>>Fakultas Olahraga dan Kesehatan</option>
    </select><br><br>
    
    <?php if($update == true):?>
    <input type="submit" name="edit" value="Ubah">
    <?php else:?>
    <input class="btn btn-info" type="submit" name="submit" value="Simpan">
    <?php endif; ?>
    </form>

    <!-- This Output -->
    <?php if(isset($_POST["submit"])) : ?>
        <div class="alert alert-primary">
            <h2>Data Dosen</h2>
            <?php
                echo "Nama : $nama <br>";
                echo "NIP : $nip <br>";
                echo "Program Studi : $prodi <br>";
                echo "Fakultas : $fakultas <br>";
            ?>
        </div>
    <?php elseif (isset($_GET["delete"])) : ?>
        <div class="alert alert-danger">
            <?php echo "$status";?>
        </div>
    <?php endif;?>
</body>
</html>