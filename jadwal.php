<?php
    include 'koneksi.php';

    $update = false;
    $id = 0;
    $idosen="";
    $idkelas="";
    $jadwal="";
    $matakuliah="";

    if(isset($_POST["submit"])){
        $iddosen = $_POST["id_dosen"];
        $idkelas = $_POST["id_kelas"];
        $jadwal = $_POST["jadwal"];
        $matakuliah = $_POST["matakuliah"];

        $sql = "INSERT INTO jadwal_kelas SET id_dosen = '$iddosen',
                                                id_kelas = '$idkelas',
                                                jadwal = '$jadwal',
                                                matakuliah ='$matakuliah'
                                                ";

        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }

    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `jadwal_kelas` WHERE id_jadwal = $id";
        $q1 = mysqli_query($koneksi, $sql);
        $row = mysqli_fetch_array($q1);

        $iddosen = $row["id_dosen"];
        $idkelas = $row["id_kelas"];
        $jadwal = $row["jadwal"];
        $matakuliah = $row["matakuliah"];

        if($idjadwal == " "){
            $status = "data kosong";
        }
    }

    if(isset($_POST["edit"])){
        $iddosen = $_POST["id_dosen"];
        $idkelas = $_POST["id_kelas"];
        $jadwal = $_POST["jadwal"];
        $fakultas = $_POST["matakuliah"];

        $sql = "UPDATE `jadwal_kelas` SET `id_dosen`='$id_dosen',`id_kelas`='$id_kelas',`jadwal`='$jadwal',`matakuliah`='$matakuliah'WHERE id_jadwal = $id";

        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }

    if(isset($_GET["delete"])){
        $id = $_GET["delete"];
        $sql = "DELETE FROM `kelas` WHERE id_jadwal=$id";

        if(mysqli_query($koneksi, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Dosen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
       <!-- Links -->
        <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="dosen.php">Form Dosen</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="kelas.php">Form Kelas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="jadwal.php">Form Jadwal</a>
        </li>
        </ul>
    </nav>
    <!-- Tampilkan Data di Database tabel kelas.. -->
    <div class="container col-8">
        <?php
            include'koneksi.php';
            $sql = "SELECT * FROM jadwal_kelas";
            $result = mysqli_query($koneksi,$sql);
        ?>
        <div class="row justify-content-center">
        <table>
            <thead>
                <tr>
                    <th>ID DOSEN</th>
                    <th>ID KELAS</th>
                    <th>Jadwal</th>
                    <th>Matakuliah</th>
                </tr>
            </thead>
                <?php while($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row["id_dosen"]?></td>
                    <td><?php echo $row["id_kelas"]?></td>
                    <td><?php echo $row["jadwal"]?></td>
                    <td><?php echo $row["matakuliah"]?></td>
                    <td>
                        <a href="kelas.php?edit=<?php echo $row["id_jadwal"];?>" class="btn btn-primary" >Edit</a>
                        <a href="kelas.php?delete=<?php echo $row["id_jadwal"];?>" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            <?php endwhile;?>
        </table>
        </div>
    </div>

    <!-- Form Input Tabel Kelas -->
    <form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <label for="formGroupExampleInput" for="Nama Kelas">ID Dosen</label><br>
    <input class="form-control" type="text" name="id_dosen" value="<?php echo $iddosen;?>" $id="id_dosen" required><br>
    <label for="formGroupExampleInput" for="Nama Kelas">ID Kelas</label><br>
    <input class="form-control" type="text" name="id_kelas" value="<?php echo $idkelas;?>" $id="id_kelas" required><br>
    <label for="formGroupExampleInput" for="Nama Kelas">Jadwal</label><br>
    <input class="form-control" type="date" name="jadwal" value="<?php echo $jadwal;?>" $id="jadwal" required><br>
    <label for="formGroupExampleInput" for="Nama Kelas">Matakuliah</label><br>
    <input class="form-control" type="text" name="matakuliah" value="<?php echo $matakuliah;?>" $id="matakuliah" required><br>




    <?php if($update == true):?>
    <input type="submit" name="edit" value="Ubah">
    <?php else:?>
    <input class="btn btn-info" type="submit" name="submit" value="Simpan">
    <?php endif; ?>
    </form> <br>

    <!-- This Output -->
    <?php if(isset($_POST["submit"])) : ?>
        <div class="alert alert-primary">
            <h2>Data Jadwal </h2>
            <?php
                echo "Nama : $iddosen <br>";
                echo "Program Studi : $idkelas <br>";
                echo "Fakultas : $jadwal <br>";
                echo "Matakuliah : $matakuliah";
            ?>
        </div>
    <?php endif; ?>
</body>
</html>